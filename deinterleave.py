import io
import sys
import argparse

def usage():
    print("Usage : %s interleavedfile evenfile oddfile" % sys.argv[0])
    sys.exit(0)
    
def main(argc, argv):
    if argc != 4:
        usage()

    parser = argparse.ArgumentParser()
    parser.add_argument("interleavedfilestr", help="filename for interleaved bytes",
                        type=str)
    parser.add_argument("evenfilestr", help="filename for even bytes",
                        type=str)
    parser.add_argument("oddfilestr", help="filename for odd bytes",
                        type=str)
    
    args = parser.parse_args()      

    interleaved = io.open( args.interleavedfilestr, "rb" )
    ba = bytearray(interleaved.read())

    even=bytearray()
    odd=bytearray()

    i=0
    for byte in ba:
        if i%2==0:
            even.append(byte)
        else:
            odd.append(byte)
        i+=1

    io.open( args.evenfilestr, "wb" ).write( even )
    io.open( args.oddfilestr, "wb").write( odd )

if __name__ == "__main__":
    main(len(sys.argv), sys.argv)